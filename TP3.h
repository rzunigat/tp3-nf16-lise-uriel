#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/////////////////////////////////////////////// DECLARATION DES STRUCTURES ///////////////////////////////////////////////

struct Transaction{

    int idEtu;
    float montant;
    char descr[20];
    struct Transaction* suivant;

};

struct Block{

    int idbloc;
    long date;
    struct Transaction* listeTransaction;
    struct Block* suivant;

};

////////////////////////////////////////////////// DECLARATION DES TYPES //////////////////////////////////////////////////

typedef struct Transaction T_Transaction;
typedef struct Block T_Block;
typedef T_Block* BlockChain;

//////////////////////////////////////////////// DECLARATION DES FONCTIONS ////////////////////////////////////////////////

T_Transaction *ajouterTransaction(int idEtu, float montant, char *descr, T_Transaction *listeTransaction);
BlockChain ajouterBlock(BlockChain bc);
float totalTransactionEtudiantBlock(int idEtu, T_Block b);
float soldeEtudiant(int idEtu, BlockChain bc);
void crediter(int idEtu, float montant, char *descr, BlockChain bc);
int payer(int idEtu, float montant, char *descr, BlockChain bc);
void consulter(int idEtu, BlockChain bc);
int transfert(int idSource, int idDestination, float montant, char *descr, BlockChain bc);

void afficher(T_Transaction t);
void afficher_tout(T_Transaction* t);
void afficher_toutB(BlockChain bc);
void afficherB(T_Block b);

T_Block chercher_Block(long date, BlockChain bc);

void liberer_blockchaine(BlockChain bc);
void liberer_trans(T_Transaction* t);

BlockChain parcourir_Blockchain(BlockChain bc);
