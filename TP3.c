#include "TP3.h"

////////////////////////////////////////////////// AJOUTER TRANSACTION //////////////////////////////////////////////////
T_Transaction *ajouterTransaction(int idEtu, float montant, char *descr, T_Transaction *listeTransaction){

    T_Transaction *ntrans;
    ntrans = (T_Transaction*)malloc(sizeof(T_Transaction));
    ntrans->idEtu=idEtu;
    ntrans->montant=montant;
    strcpy(ntrans->descr,descr);

    if(listeTransaction==NULL){

        ntrans->suivant = NULL;

    }

    else{

        ntrans->suivant=listeTransaction;
    }


    return ntrans;

}

///////////////////////////////////////////////////// AJOUTER BLOCK /////////////////////////////////////////////////////

BlockChain ajouterBlock(BlockChain bc){

    T_Block* blockt, *aux;

    blockt = (T_Block*) malloc(sizeof(T_Block));
    blockt->listeTransaction=NULL;
    blockt->suivant=NULL;

    if(bc==NULL){

        blockt->date=20191021;
        blockt->idbloc=0;
        bc=blockt;

    }

    else{
            aux = bc;

        while(aux->suivant!=NULL){

            aux=aux->suivant;

        }

        blockt->date=(aux->date)+1;
        blockt->idbloc=(aux->idbloc)+1;
        aux->suivant=blockt;

    }

return bc;
}

///////////////////////////////////////////////// TOTAL TRANSACTION D'UN JOUR /////////////////////////////////////////////////

float totalTransactionEtudiantBlock(int idEtu, T_Block b){

float somme=0;

if(b.listeTransaction->idEtu==idEtu) somme+=b.listeTransaction->montant;

while(b.listeTransaction->suivant!=NULL){

    b.listeTransaction=b.listeTransaction->suivant;
    if(b.listeTransaction->idEtu==idEtu) somme += b.listeTransaction->montant;

}

return somme;

}

//////////////////////////////////////////////////////// SOLDE ETUDIANT ////////////////////////////////////////////////////////

float soldeEtudiant(int idEtu, BlockChain bc){

 float somme=0;

while(bc!=NULL){

    if(bc->listeTransaction!=NULL){
            if(bc->listeTransaction->idEtu==idEtu) somme += totalTransactionEtudiantBlock(idEtu, *bc);
    }

    else return somme;

    bc=bc->suivant;

}

return somme;

}

/////////////////////////////////////////////////////////// CREDITER ///////////////////////////////////////////////////////////

void crediter(int idEtu, float montant, char *descr, BlockChain bc){

BlockChain aux;

aux=parcourir_Blockchain(bc);

aux->listeTransaction=ajouterTransaction(idEtu, montant, descr, aux->listeTransaction);


}

/////////////////////////////////////////////////////////// PAYER ///////////////////////////////////////////////////////////

int payer(int idEtu, float montant, char *descr, BlockChain bc){


BlockChain aux;

aux=parcourir_Blockchain(bc);

if(soldeEtudiant(idEtu, bc)< montant) return 0;

else{

    aux->listeTransaction=ajouterTransaction(idEtu, montant*(-1), descr, aux->listeTransaction);
    return 1;

}

}

///////////////////////////////////////////////////////// CONSULTER /////////////////////////////////////////////////////////

void consulter(int idEtu, BlockChain bc){

int count=0;
printf("Le solde de l'etudiant %d est: %f\n\n", idEtu, soldeEtudiant(idEtu, bc));

while(bc!=NULL){

while(count<5 || bc==NULL){

    if(idEtu==bc->listeTransaction->idEtu){

        afficher_tout(bc->listeTransaction);
        count++;

    }

    bc->listeTransaction=bc->listeTransaction->suivant;


}

 bc=bc->suivant;

}

}





///////////////////////////////////////////////////////// TRANSFERT /////////////////////////////////////////////////////////

int transfert(int idSource, int idDestination, float montant, char *descr, BlockChain bc){

if(soldeEtudiant(idSource, bc)< montant) return 0;

else {

    payer(idSource, montant, descr, bc);
    crediter(idDestination, montant, descr, bc);
    return 1;


}
}

/////////////////////////////////////////////////// AFFICHER TRANSACTIONS ////////////////////////////////////////////////////

void afficher(T_Transaction t){

printf("\t  %d\t\t %.2f\t\t %s\t\n",t.idEtu,t.montant,t.descr);

}

void afficher_tout(T_Transaction* t){
while(t!=NULL){
    afficher(*t);
    t=t->suivant;
}
}

///////////////////////////////////////////// AFFICHER TRANSACTIONS POUR UN ETUDIANT /////////////////////////////////////////////

void afficherE(T_Transaction t, int IdEtu){

if(t.idEtu==IdEtu) printf("\t  %d\t\t %.2f\t\t %s\t\n",t.idEtu,t.montant,t.descr);

}

void afficher_toutE(T_Transaction* t, int IdEtu){

while( t!=NULL){


    afficherE(*t, IdEtu);
     t=t->suivant;

}
}

///////////////////////////////////////////////////// AFFICHER BLOCK //////////////////////////////////////////////////////

void afficher_toutB(BlockChain bc){
while(bc!=NULL){
    afficherB(*bc);
    bc=bc->suivant;
}
}

void afficherB(T_Block b){

printf("ID: %d\t Date: %ld \n", b.idbloc, b.date);

}

///////////////////////////////////////////////////// CHERCHER BLOCK //////////////////////////////////////////////////////

T_Block chercher_Block(long date, BlockChain bc){

    T_Block aux;

    while(bc!=NULL){

        if(bc->date==date) {

            aux.date=date;
            aux.idbloc=bc->idbloc;
            aux.listeTransaction=bc->listeTransaction;
            aux.suivant=NULL;
            return aux;

        }

        bc=bc->suivant;

    }

return aux;

}

///////////////////////////////////////////////////// LIBERER MEMOIRE //////////////////////////////////////////////////////

void liberer_blockchaine(BlockChain bc){

    while(bc!=NULL){

        free(bc);
        bc=bc->suivant;
    }

}


void liberer_trans(T_Transaction* t){

while(t!=NULL){

    free(t);
    t=t->suivant;

}

}

///////////////////////////////////////////////// PARCOURIR BLOCKCHAINE //////////////////////////////////////////////////

BlockChain parcourir_Blockchain(BlockChain bc){

while(bc->suivant!=NULL){

  bc=bc->suivant;

}

return bc;

}



