#include "TP3.h"

T_Transaction* Ltransaction=NULL;
BlockChain LBlock=NULL;


int main(){

    int choix=-1, idEtu, idDes;
    float montant;
    long date;
    char descri[30];

    while(choix!=9){

            system("cls");
            printf("\n\t\t\t Bienvenue a notre programme de administration des EATcoins\n\n");
            printf("----------------------------------------------------------------------------------------------------------\n\n");

            while(choix<=0 || choix>9){

                printf("Selectionner une fonction : \n\n");
                printf("\t1- Afficher la liste des blocs\n");
                printf("\t2- Afficher tous las transactions d'un jour\n");
                printf("\t3- Afficher tous las transactions d'un jour pour un etudiant\n");
                printf("\t4- Afficher l'historique pour un etudiant\n");
                printf("\t5- Crediter un compte\n");
                printf("\t6- Payer un repas\n");
                printf("\t7- Transferer des EATcoins entre deux etudiants\n");
                printf("\t8- Augmenter date\n");
                printf("\t9- QUITTER\n\n");
                printf("Votre choix ?\t ");
                scanf("%d",&choix);
        }

        switch(choix){

            case 1:         //AFFICHER LISTES DES BLOCKS //

                system("cls");

                printf("\n\t\t\t Afficher la liste des blocs\n\n");
                printf("----------------------------------------------------------------------------------------------------------\n\n");

                afficher_toutB(LBlock);
                system("pause");

                choix=-1;
            break;


            case 2:          //AFFICHER TRANSACTIONS D'UN BLOCK //

                system("cls");

                printf("\n\t\t\t Tous les transactions d'un jour \n\n");
                printf("----------------------------------------------------------------------------------------------------------\n\n");

                if(LBlock==NULL) printf("Il n'y a pas des transactions!!\n\n");

                else{

                    printf("Ecrivez le jour (AAAAMMJJ):\t");
                    scanf("%ld", &date);
                    printf("\n\tidEu\t\tMontant\t\t Description\n\n");
                    afficher_tout(chercher_Block(date, LBlock).listeTransaction);
                    printf("\n");
                }

                system("pause");
                choix=-1;


            break;

            case 3:           // AFFICHER TRANSACTIONS D'UN BLOCK POUR UN ETUDIANT //

                system("cls");

                printf("\n\t\t\t Tous les transactions de un jour pour un etudiant\n\n");
                printf("----------------------------------------------------------------------------------------------------------\n\n");

                if(LBlock==NULL) printf("Il n'y a pas des transactions!!\n\n");

                else{

                    printf("Choisissez le idEtu:\t");
                    scanf("%d", &idEtu);

                    printf("Choisissez la date:\t");
                    scanf("%ld", &date);

                    printf("\n\nLe solde de l'etudiant %d est: %.2f\n\n", idEtu, soldeEtudiant(idEtu, LBlock));
                    printf("\n\tidEu\t\tMontant\t\t Description\n\n");
                    afficher_toutE(chercher_Block(date, LBlock).listeTransaction,idEtu);
                    printf("\n");

                }

                system("pause");
                choix=-1;


            break;

            case 4:                       //HISTORIQUE D'UN ETUDIANT//

                system("cls");

                printf("\n\t\t\t Historique d'un etudiant\n\n");
                printf("----------------------------------------------------------------------------------------------------------\n\n");

                printf("Choisissez le idEtu:\t");
                scanf("%d", &idEtu);

                consulter(idEtu, LBlock);


                system("pause");
                choix=-1;


            break;

            case 5:          //CREDITER UN COMPTE //

                system("cls");

                printf("\n\t\t\t Crediter un compte\n\n");
                printf("----------------------------------------------------------------------------------------------------------\n\n");

                printf("Ecrivez l'id de l'etudiant:\t");
                scanf("%d", &idEtu);

                printf("Ecrivez le montant a crediter:\t");
                scanf("%f", &montant);

                if(LBlock==NULL){

                    LBlock = ajouterBlock(LBlock);


                }

                crediter(idEtu, montant, "rechargements", LBlock);

                system("pause");

                choix=-1;


            break;

            case 6:           // PAYER UN REPAS //

                system("cls");

                printf("\n\t\t\t Payer un repas \n\n");
                printf("----------------------------------------------------------------------------------------------------------\n\n");

                printf("Ecrivez l'id de l'etudiant:\t");
                scanf("%d", &idEtu);

                printf("Ecrivez le montant a crediter:\t");
                scanf("%f", &montant);

                printf("Description:\t");
                fflush(stdin);
                fgets(descri, 30, stdin);

                if(payer(idEtu,montant,descri, LBlock)==0) printf("\n\nSolde insufficent!!!\n\n");

                else printf("\n\nTransaction accepte!!!\n\n");

                system("pause");

                choix=-1;


            break;

            case 7:           //TRANSFERER D'UN ETUDIANT A UN AUTRE //

                system("cls");

                printf("\n\t\t\t Transferer EATCoins\n\n");
                printf("----------------------------------------------------------------------------------------------------------\n\n");

                printf("Ecrivez ton id d'etudiant:\t");
                scanf("%d", &idEtu);

                printf("Ecrivez l'id de l'etudiant destinataire:\t");
                scanf("%d", &idDes);


                printf("Ecrivez le montant a crediter:\t");
                scanf("%f", &montant);

                printf("Description:\t");
                fflush(stdin);
                fgets(descri, 30, stdin);

                if(transfert(idEtu, idDes, montant, descri, LBlock)==0) printf("\n\nSolde insufficent!!!\n\n");

                else printf("\n\nTranference accepte!!!\n\n");

                system("pause");

                choix=-1;


            break;

            case 8:           //AJOUTER UN AUTRE BLOCK //

                system("cls");
                printf("\n\nAugmenter date\n\n");
                printf("----------------------------------------------------------------------------------------------------------\n\n");

                LBlock=ajouterBlock(LBlock);

                printf("Date ajoutee!!\n\n");
                system("pause");

                choix=-1;


            break;

            case 9:             // QUITTER  //

                system("cls");
                printf("\n\nAu revoir\n\n");
                printf("----------------------------------------------------------------------------------------------------------\n\n");

                liberer_blockchaine(LBlock);
                liberer_trans(Ltransaction);


            break;


        }


    }

}